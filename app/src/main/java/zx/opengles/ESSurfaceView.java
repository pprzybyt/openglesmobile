package zx.opengles;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ViewConfiguration;

/**
 @author Marek Kulawiak
 */

class ESSurfaceView extends GLSurfaceView
{
	protected GLRenderer renderer=null;
    private long startClickTime;
    private float lastY = -1;
    private float lastX = -1;


    public ESSurfaceView(Context context)
    {
        super(context);

        // Stworzenie kontekstu OpenGL ES 2.0.
        setEGLContextClientVersion(2);

        // Przypisanie renderera do widoku.
        renderer = new GLRenderer();
        renderer.setContext(getContext());
        setRenderer(renderer);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        float currentY = event.getY();
        float currentX = event.getX();

        if (lastX != -1){
            if (renderer != null){
                float diffX = currentX - lastX;
                float diffY = currentY - lastY;

                renderer.camera[3] += diffX / 1000.0f;
                renderer.camera[4] -= diffY / 1000.0f;
            }
        }

        lastY = currentY;
        lastX = currentX;

        if (event.getAction() == MotionEvent.ACTION_UP) {
            lastY = -1;
            lastX = -1;
        }

        return true;
    }

    public GLRenderer getRenderer()
    {
    	return renderer;
    }
}
